package dev.sanero.microservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;

import reactor.core.publisher.Mono;

@RestController("/")
public class HomeController {
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	WebClient.Builder builder;
	
	@Autowired
	Environment env;
	
	@GetMapping("/")
	public String home() {
		
		return "Gallery service running in port " + env.getProperty("local.server.port");
	}
	
	@HystrixCommand(fallbackMethod = "fallback")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/gallery")
	public Gallery getGallery() {
		Gallery gallery = new Gallery();
		ResponseEntity<List> responseEntity = restTemplate.getForEntity(env.getProperty("image.service") + "/images", List.class);
		if (HttpStatus.OK.equals(responseEntity.getStatusCode()) && responseEntity.hasBody()) {
			List<Object> images = responseEntity.getBody();
			gallery.setImages(images);
		}
		
		return gallery;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/gallery-web-client")
	public Gallery getGalleryUsingWebClient() {
		Gallery gallery = new Gallery();
		Mono<ClientResponse> response = builder.build().get().uri(env.getProperty("image.service") + "/images").exchange();
		ClientResponse clientResponse = response.block();
		if (HttpStatus.OK.equals(clientResponse.statusCode())) {
			Mono<List> images = clientResponse.bodyToMono(List.class);
			gallery.setImages(images.block());
		}
		return gallery;
	}
	
	public Gallery fallback(Throwable hystrixCommand) {
		return null;
	}
}
