package dev.sanero.microservice.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityTokenConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	private JwtConfig jwtConfig;
	
	@Bean
	public JwtConfig jwtConfig() {
		return new JwtConfig();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		// make sure we use stateless session; session won't be used to store user's state.
 	    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) 
 	    .and()
 	    // handle an authorized attempts 
 	    .exceptionHandling().authenticationEntryPoint((req, res, err) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED)) 
 	    .and()
 	    .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
 	    // authorization requests config
 	    .authorizeRequests()
 	    .antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()
 	    .antMatchers("/gallery" + "/admin/**").hasRole("ADMIN")
 	    // Any other request must be authenticated
 	    .anyRequest().authenticated();
	}
}
