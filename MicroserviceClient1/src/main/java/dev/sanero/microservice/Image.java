package dev.sanero.microservice;

public class Image {
	private String id;
	
	private String title;
	
	private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public Image() {
		super();
	}

	public Image(String id, String title, String url) {
		super();
		this.id = id;
		this.title = title;
		this.url = url;
	}


	@Override
	public String toString() {
		return "Image [id=" + id + ", title=" + title + ", url=" + url + "]";
	}


	public static class Builder {
		private String id;
		
		private String title;
		
		private String url;
		
		public Builder id(String id) {
			this.id = id;
			return this;
		} 
		
		public Builder title(String title) {
			this.title = title;
			return this;
		} 
		
		public Builder url(String url) {
			this.url = url;
			return this;
		} 
		
		public Image build() {
			Image image = new Image(this.id, this.title, this.url);
			return image;
		}
	}
}
