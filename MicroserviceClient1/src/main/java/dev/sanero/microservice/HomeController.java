package dev.sanero.microservice;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {
	@Autowired
	Environment env;
	
	@RequestMapping("/images")
	public List<Image> getImages() {
		List<Image> images = new ArrayList<>();
		
		images.add(new Image.Builder().id(UUID.randomUUID().toString()).title("Image1").url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlPjg6k3D9gcbTZPNvgfD9mbX-a9Oa-8Q0Ew&usqp=CAU").build());
		images.add(new Image.Builder().id(UUID.randomUUID().toString()).title("Image2").url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTU1dFR0DR4d0wILeQRaH1oo1Dlsag-Gp2dXg&usqp=CAU").build());
		images.add(new Image.Builder().id(UUID.randomUUID().toString()).title("Image3").url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTntlma5HASL0HAM-KiC01A-JX4MxKousAA6A&usqp=CAU").build());
		
		return images;
	}
}
